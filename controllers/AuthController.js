const requestJSON = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneyu/collections/users/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../utils/crypt');
const authJWT = require('../utils/auth');
const emailService = require('../services/mail');
const env = process.env.ENV;

function loginv1(req,res){
  console.log ("POST /easymoney/v1/login");
  let email = req.body.email;
  let password = req.body.password;
  console.log("El email del usuario:" + email);
  console.log("El password del usuario:" + password);
  if (!_checkLoginField(req.body)){
    console.log("_checkLoginField false");
    responseJSON = {"msg": "Error login faltan parametros"};
    res.status(400);
    res.send(responseJSON);
  }
  else{

    let query = 'q={"email":"' + email + '"}';
    // let mlabUrl = "user" + "?" + query +"&" +  mlabAPIKey;
    let mlabUrl = "?" + query +"&" +  mlabAPIKey;
    let responseJSON = {};
    let httpClient = requestJSON.createClient(mlabBaseURL);
    httpClient.get(mlabUrl,
      function(err, resMLab, body) {
        if (err){
            res.status(500);
            responseJSON = {"msg": "Error obteniendo usuarios"};
            res.send(responseJSON);
        } else {
          if (body.length > 0) {
            if (crypt.checkpassword(password,body[0].password)){
              console.log("contraseña correcta");
              let id = body[0].id;
              let firstName = body[0].first_name;
              let lastName = body[0].last_name;
              let gender = body[0].gender;
              let email = body[0].email;
              let avatar = body[0].avatar;
              let query = 'q={"id":' + body[0].id + '}';
              let mlabUrl = "?"+ query +"&" +  mlabAPIKey;
              console.log("mlabUrl" + mlabUrl);
              let putBody = '{"$set":{"logged":true}}';
              let httpClient = requestJSON.createClient(mlabBaseURL);
              httpClient.put(mlabUrl, JSON.parse(putBody),
                function(err, resMLab, body) {
                  console.log("Usuario logged");
                  (!err) ? responseJSON = {"msg": "Usuario logged"} : responseJSON = {"msg": "Usuario not logged"};
                  responseJSON.id = id;
                  responseJSON.firstName = firstName;
                  responseJSON.lastName = lastName;
                  responseJSON.gender = gender;
                  responseJSON.email = email;
                  responseJSON.avatar = avatar;
                  responseJSON.token = authJWT.createToken(id);
                  console.log(responseJSON);
                  res.send(responseJSON);
                }
              );
            }
            else{
              console.log("contraseña incorrecta");
              responseJSON = {"msg": "Login incorrecto"}
              res.status(401);
              res.send(responseJSON);
            }

          } else{
            console.log("Usuario no encontrado");
            responseJSON = {"msg": "Usuario incorrecto"};
            res.status(401);
            res.send(responseJSON);
          }
        }
      }
    )
  }
}

function logoutv1(req,res){
  console.log ("POST /easymoney/v1/logout/:id");
  let idUser = req.params.id;
  console.log ("idUser: " + idUser);

  let query = 'q={"id":' + idUser + '}';
  // let mlabUrl = "user" + "?" + query +"&" +  mlabAPIKey;
  let mlabUrl = "?" + query +"&" +  mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.get(mlabUrl,
    function(err, resMLab, body) {
      if (err){
          res.status(500);
          responseJSON = {"msg": "Error obteniendo usuarios"};
          res.send(responseJSON);
      } else {
        if (body.length > 0) {
          if (body[0].logged){
            console.log("usuario logged");
            let id = body[0].id;
            let query = 'q={"id":' + body[0].id + '}';
            let mlabUrl = "?"+ query +"&" +  mlabAPIKey;
            console.log("mlabUrl" + mlabUrl);
            let putBody = '{"$set":{"logged":""}}';
            let httpClient = requestJSON.createClient(mlabBaseURL);
            httpClient.put(mlabUrl, JSON.parse(putBody),
              function(err, resMLab, body) {
                console.log("Usuario log out");
                (!err) ? responseJSON = {"msg": "Logout correcto"} :
                         responseJSON = {"msg": "Logout incorrecto"};
                responseJSON.id = id;
                res.send(responseJSON);
              }
            );
          }
          else{
            console.log("Usuario no logado");
            responseJSON = {"msg": "Usuario no logado"}
            responseJSON.id = body[0].id;
            res.status(404);
            res.send(responseJSON);
          }

        } else{
          responseJSON = {"msg": "Usuario no encontrado"};
          res.status(404);
          res.send(responseJSON);
        }
      }
    }
  )
}

function signUpv1 (req,res){
  console.log("POST /easymoney/v1/signup");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);
  if (!_checkSignUpField(req.body)){
    console.log("_checkSignUpField false");
    responseJSON = {"msg": "Error signup faltan parametros"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
    console.log("mlabUrl" + mlabUrl);
    var responseJSON = {};
    let httpClient = requestJSON.createClient(mlabBaseURL);
    httpClient.get(mlabUrl,
      function(err, resMLab, body) {
        console.log("get last id");
        if (err){
            console.log("Eror 500 al consultar el ultimo id");
            res.status(500);
            responseJSON = {"msg": "Error al consultar el ultimo id"};
            res.send(responseJSON);
        } else {
            let id = parseInt(body[0].id) + 1;
            console.log("Siguiente id: " + id);
            let newUser = {
              "id" : id,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "password": crypt.hash(req.body.password),
              "gender": req.body.gender,
              "avatar": req.body.avatar,
              "logged": true
            }
            let avatarImgPath = (process.env.ENV == "live") ? "/easymoneybank/resources/images/" : "/resources/images/";
            if (req.body.avatar == "" || req.body.avatar == undefined)
               newUser.avatar =  (req.body.gender == "man") ? avatarImgPath + "hombre.png" : avatarImgPath + "chica.png";
            let mlabUrl = "?" + mlabAPIKey;
            console.log("mlabUrl" + mlabUrl);
            console.log("newUser:");
            console.log(newUser);
            var responseJSON = {};
            let httpClient = requestJSON.createClient(mlabBaseURL);
            httpClient.post(mlabUrl, newUser,
              function(err, resMLab, body) {
                if (err){
                    res.status(500);
                    responseJSON = {"msg": "Error al crear el usuario"};
                    res.send(responseJSON);
                } else {
                  res.status(201); //created
                  responseJSON.msg = "Usuario insertado correctamente";
                  responseJSON.id = newUser.id;
                  responseJSON.firstName = newUser.first_name;
                  responseJSON.lastName = newUser.last_name;
                  responseJSON.gender = newUser.gender;
                  responseJSON.email = newUser.email;
                  responseJSON.avatar = newUser.avatar;
                  responseJSON.token = authJWT.createToken(newUser.id);
                  console.log(responseJSON);
                  res.send(responseJSON);
                }
              }
            );
          }
        }
      );
  }
}

function editUserv1 (req,res){
  console.log("PUT /easymoney/v1/users/:id");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  if (!_checkSignUpField(req.body)){
    console.log("_checkSignUpField false");
    responseJSON = {"msg": "Error signup faltan parametros"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    let editUser = {
      "id" : req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password),
      "gender" : req.body.gender,
      "avatar" : req.body.avatar,
      "logged" : true
    }
    // let mlabUrl = "?" + mlabAPIKey;
    let query = 'q={"id":' + req.body.id + '}';
    let mlabUrl = "?" + query +"&" +  mlabAPIKey;
    console.log("mlabUrl" + mlabUrl);
    var responseJSON = {};
    let httpClient = requestJSON.createClient(mlabBaseURL);
    httpClient.put(mlabUrl, editUser,
      function(err, resMLab, body) {
        if (err){
            res.status(500);
            responseJSON = {"msg": "Error al editar el usuario"};
            res.send(responseJSON);
        } else {
          res.status(200); //edited
          responseJSON.msg = "Usuario editado correctamente";
          responseJSON.id = editUser.id;
          responseJSON.firstName = editUser.first_name;
          responseJSON.lastName = editUser.last_name;
          responseJSON.gender = editUser.gender;
          responseJSON.email = editUser.email;
          responseJSON.avatar = editUser.avatar;
          //responseJSON.token = authJWT.createToken(newUser.id);
          res.send(responseJSON);
        }
      }
    );
  }
}


function forgotv1 (req,res){
  console.log("POST /easymoney/v1/forgot");
  let email = req.body.email;
  console.log(email);
  let responseJSON = {};
  if (!email){
    console.log("email false");
    responseJSON = {"msg": "Error email no informado"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    let query = 'q={"email":"' + email + '"}';
    let mlabUrl = "?" + query +"&" +  mlabAPIKey;
    let httpClient = requestJSON.createClient(mlabBaseURL);
    
    httpClient.get(mlabUrl,
      function(err, resMLab, body) {
        if (err){
          console.log("Error 500..");
          res.status(500);
          responseJSON = {"msg": "Error obteniendo usuario"};
          res.send(responseJSON);
        } else {
          if (body.length > 0) {
            console.log("usuario encontrado");
            
            let id = body[0].id;
            let msg = '<h3>Cambio de contraseña</h3>';
            msg += '<p>Para cambiar la contraseña pulse el siguiente ';
            msg += '<a href="https://s3-eu-west-1.amazonaws.com/easymoneybank/index.html#!/changepassword?id=' + id + '">enlace</a>';
            msg += '</p>';

            console.log("id:" + id);
            console.log("msg:" + msg);
          
            emailService.send(email,'easymoneybanktechu@gmail.com','Easy Money Bank - Cambio de contraseña',msg);
            res.status(200);
            responseJSON = {"msg": "Email con cambio de contraseña enviado"};
            res.send(responseJSON);
          }
          else {
            console.log("Usuario no encontrado");
            responseJSON = {"msg": "Usuario no encontrado"}
            res.status(404);
            res.send(responseJSON);
          }
        }
      });
    }
  }

function _checkLoginField(params){
  console.log("_checkLoginField");

  if (params.email == undefined || params.email == ""
      || params.password == undefined || params.password == "")
    return false;
  return true;
}
function _checkSignUpField(params){
  console.log("_checkSignUpField");
  if (params.first_name == undefined || params.first_name == ""
      || params.last_name == undefined || params.last_name == ""
      || params.email == undefined || params.email == "")
    return false;
  return true;
}
module.exports.forgotv1 = forgotv1;
module.exports.signUpv1 = signUpv1;
module.exports.loginv1 = loginv1;
module.exports.logoutv1 = logoutv1;
module.exports.editUserv1 = editUserv1;
