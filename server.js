require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 // No producciserviceón!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers","*");
 next();
}
app.use(enableCORS);

const authJWT = require('./utils/auth');

const authController = require('./controllers/AuthController');

app.use(express.json());

app.post("/easymoney/v1/login", authController.loginv1);
app.post("/easymoney/v1/logout/:id", authController.logoutv1);
app.post("/easymoney/v1/signup", authController.signUpv1);
app.post("/easymoney/v1/forgot", authController.forgotv1);
app.put("/easymoney/v1/users/:id", authJWT.isAuthenticated, authController.editUserv1);

app.listen(port,
  function(){
    console.log ("Auth Services of EasyMoney Bank is running !!! In Port:" + port);
  }
);
