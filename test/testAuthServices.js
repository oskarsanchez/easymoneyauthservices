const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require('chai-http');
const requestJSON = require('request-json');

require('dotenv').config();

chai.use(chaihttp);

const should = chai.should();

const auth = require("../utils/auth");

const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneyu/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const URL_AUTH_SERVICES = process.env.URL_AUTH_SERVICES || "http://localhost:3020/easymoney/v1/";


/*function sleep(ms){
   return new Promise(resolve=>{
       setTimeout(resolve,ms)
   });
}*/

describe ('Test de API de Auth Services',
  function(){
    before(function() {
      // runs before all tests in this block
      const server = require('../server');
      //  deleteCollecion("movements");
      //  insertManyCollection("movements", "./movements.json");
      // ver cuando investigue promesas
      //  await sleep(1000);

    });

    after(function() {
      // runs after all tests in this block

    });

    beforeEach(function() {
      // runs before each test in this block
    });
    afterEach(function() {
      // runs after each test in this block
    });

    it('Test signup new user test@gmail.com', function(done){
      let dataJSON = {
         "first_name" : "Test Name",
         "last_name" : "Test Last Name",
         "email" : "test@gmail.com",
         "password" : "test"
       }

      chai.request(URL_AUTH_SERVICES)
        .post('signup')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST signup user test");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(201);
            res.body.should.have.property("msg");
            res.body.should.have.property("token");
            res.body.should.have.property("id");
            done();
          }
        )
      }
    ),
    it.skip('Test signup a user that exist, so email exist', function(done){
      let dataJSON = {
         "first_name" : "Test Name",
         "last_name" : "Test Last Name",
         "email" : "test@gmail.com",
         "password" : "test"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('signup')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST signup");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test signup a user that mandatory property not send', function(done){
      let dataJSON = {
         "id":999999,
         "first_name" : "Test Name",
         "last_name" : "Test Last Name",
         "email" : "",
         "password" : "test"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('signup')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST signup without email");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test edit user', function(done){
      let token = auth.createToken (999999);
      let dataJSON = {
         "id": 999999,
         "first_name": "Test Edit Name",
         "last_name": "Test Edit Last Name",
         "email": "testedit@gmail.com",
         "password": "testedit",
         "gender": "male",
         "avatar": "avatar"
       }
      chai.request(URL_AUTH_SERVICES)
        .put('users/'+dataJSON.id)
        .set('authorization', 'JWT ' + token)
        .set('user_id', 999999)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing PUT edit user test");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.have.property("msg");
            res.body.should.have.property("id");
            res.body.should.have.property("firstName");
            res.body.should.have.property("lastName");
            res.body.should.have.property("email");
            res.body.should.have.property("gender");
            res.body.should.have.property("avatar");
            done();
          }
        )
      }
    ),
    it('Test login a user that mandatory property not send', function(done){
      let dataJSON = {
         "email" : "",
         "password" : "test"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('login')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST login without email");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test login a user', function(done){
      let dataJSON = {
         "email" : "test@gmail.com",
         "password" : "test"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('login')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST login");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.have.property("msg");
            res.body.should.have.property("token");
            res.body.should.have.property("id");
            done();
          }
        )
      }
    ),
    it('Test forgot a user password that email user exist', function(done){
      let dataJSON = {
         "email" : "testedit@gmail.com"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('forgot')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST forgot user exists");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test forgot a user password that email user doesnt exist', function(done){
      let dataJSON = {
         "email" : "oscar.sanchez@gmail.com"
       }
      chai.request(URL_AUTH_SERVICES)
        .post('forgot')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST logout user doesnt exist");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(404);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test forgot a user password without email user', function(done){
      let dataJSON = {
         "email" : ""
       }
      chai.request(URL_AUTH_SERVICES)
        .post('forgot')
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST logout without email");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    )
  }
);

function deleteCollecion(collection){
  console.log("deleteCollecion... collection:" + collection );
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.put(mlabUrl, [],
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}

function insertManyCollection(collection,file){
  console.log("insertManyCollection... collection:" + collection + ", file:" + file);
  let movements = require(file);
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.post(mlabUrl, movements,
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}
