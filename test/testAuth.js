const mocha = require ('mocha');
const chai = require ('chai');
const should = chai.should();

const auth = require("../utils/auth");

describe ('Test de Auth library',
  function(){
    it('Test createToken JWT',
      function(){
        let token = auth.createToken(1);
        let isToken = auth.verifyToken(token,1);
        isToken.should.be.true;
        console.log(token);
      }
    )
    it('Test verify invalid token JWT',
      function(){
        let token = auth.createToken(1);
        let isToken = auth.verifyToken(token,2);
        isToken.should.be.false;
        console.log(token);
      }
    )
  }
)
