const sgMail = require('@sendgrid/mail');

const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY;

function send(to,from, subject, body, format){
  console.log("send");
  
  sgMail.setApiKey(SENDGRID_API_KEY);

  const msg = {
    to: to,
    from: from,
    subject: subject
  };
  (format == 'text') ? msg.txt = body : msg.html = body;
  console.log(msg);
  sgMail.send(msg);
}

module.exports.send = send;
