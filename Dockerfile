# Dockerfile Easy Money Bank Auth Services

# Imagen raiz
FROM node

# Carpeta trabajo
WORKDIR /easyMoneyAuthServices

# Añado archivos de mi aplicación
ADD . /easyMoneyAuthServices

# Establecemos variables de entorno

# Instalo los paquetes necesarios
RUN npm install --production

# Exponemos el puerto
EXPOSE 3000

CMD ["node","server.js"]
