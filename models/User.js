const user = {
  "id" : Number,
  "firstName" : String,
  "lastName" : String,
  "email": String,
  "logged" : Boolean,
  "gender" : String,
  "avatar" : String
}
module.exports.user = user;
